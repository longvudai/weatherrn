import { Navigation } from 'react-native-navigation'

// MARK: - Screen ID
import { SCREENID } from './ScreenId'

import { registerScreens } from './Configure'

function startApp() {
  registerScreens()

  Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: SCREENID.MAINSCREEN,
                options: {
                  topBar: {
                    title: {
                      text: 'Weather App'
                    }
                  }
                }
              }
            }
          ],
          options: {
            topBar: {
              visible: true
            }
          }
        }
      }
    })
  })
}

// MARK: - Export
module.exports = {
  startApp
}
