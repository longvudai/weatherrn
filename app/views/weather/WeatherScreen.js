import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ImageBackground,
  StatusBar,
  ActivityIndicator
} from 'react-native'

import PropTypes from 'prop-types'

import getImageForWeather from '../../modules/getImageForWeather'

// MARK: - Redux
import { fetchWeather } from '../../redux/actions/weather.actions'
import { connect } from 'react-redux'
import store from '../../redux/store'

class WeatherScreen extends Component {
  state = {
    notify: false,
    status: ''
  }

  static propTypes = {
    woeid: PropTypes.number.isRequired
  }

  unsubscribe = null

  weatherData = { weather_state_name: 'Heavy Rain', the_temp: 17.065 }

  handleFetchWeather = () => {
    const { data, error } = store.getState().weather

    if (error == null) {
      this.weatherData = data
    }

    this.setState({
      notify: error != null ? true : false,
      status: error != null ? error.message : ''
    })
  }

  _fetchWeather = () => {
    const { woeid, fetchWeather } = this.props
    fetchWeather(woeid)
  }

  componentDidMount = async () => {
    this.unsubscribe = store.subscribe(this.handleFetchWeather)

    this._fetchWeather()
  }

  componentWillUnmount = () => {
    this.unsubscribe()
  }

  render() {
    const { loading } = this.props
    const { notify, status } = this.state

    const { the_temp, weather_state_name } = this.weatherData

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="default" />
        <ImageBackground
          style={styles.imageContainer}
          imageStyle={styles.imageStyle}
          source={getImageForWeather(weather_state_name)}>
          <ActivityIndicator animating={loading} size="large" color="#0000ff" />
          {notify && (
            <Text
              style={[styles.textStyle, styles.smallText, styles.errorText]}>
              {status}
            </Text>
          )}

          {!loading && !notify && (
            <View>
              <Text style={[styles.textStyle, styles.smallText]}>
                {weather_state_name}
              </Text>
              <Text style={[styles.textStyle, styles.largeText]}>
                {`${Math.round(the_temp)} °`}
              </Text>
            </View>
          )}
        </ImageBackground>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'powderblue',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  imageStyle: {
    resizeMode: 'cover'
  },

  textStyle: {
    textAlign: 'center',
    color: 'white'
    // backgroundColor: 'red'
  },

  largeText: {
    fontSize: 44
  },

  smallText: {
    fontSize: 18
  },
  errorText: {
    color: 'red'
  }
})

const mapStateToProps = state => ({
  weatherData: state.weather.data,
  loading: state.weather.loading,
  error: state.weather.error
})

const mapDispatchToProps = {
  fetchWeather
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WeatherScreen)
