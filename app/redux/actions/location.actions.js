import { API } from '../../services/api'

const FETCH_LOCATION_ID_BEGIN = 'FETCH_LOCATION_ID_BEGIN'
const FETCH_LOCATION_ID_SUCCESS = 'FETCH_LOCATION_ID_SUCCESS'
const FETCH_LOCATION_ID_FAILURE = 'FETCH_LOCATION_ID_FAILURE'

// const mockLocationIdData = [
//   {
//     title: 'San Francisco',
//     location_type: 'City',
//     woeid: 2487956,
//     latt_long: '37.777119, -122.41964'
//   },
//   {
//     title: 'San Diego',
//     location_type: 'City',
//     woeid: 2487889,
//     latt_long: '32.715691,-117.161720'
//   },
//   {
//     title: 'San Jose',
//     location_type: 'City',
//     woeid: 2488042,
//     latt_long: '37.338581,-121.885567'
//   }
// ]

const fetchLocationIdBegin = () => ({
  type: FETCH_LOCATION_ID_BEGIN
})

const fetchLocationIdSuccess = locationData => ({
  type: FETCH_LOCATION_ID_SUCCESS,
  payload: locationData
})

const fetchLocationIdFailure = error => ({
  type: FETCH_LOCATION_ID_FAILURE,
  payload: error
})

const fetchLocationId = city => {
  return dispatch => {
    dispatch(fetchLocationIdBegin())
    API.metaweather
      .fetchLocationId(city)
      .then(result => {
        dispatch(fetchLocationIdSuccess(result))
      })
      .catch(e => {
        dispatch(fetchLocationIdFailure(e))
      })
  }
}

module.exports = {
  // TYPE ACTIONS
  FETCH_LOCATION_ID_BEGIN,
  FETCH_LOCATION_ID_SUCCESS,
  FETCH_LOCATION_ID_FAILURE,

  fetchLocationId
}
