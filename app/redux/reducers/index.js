import { combineReducers } from 'redux'

// MARK: - Reducers
import locationReducer from './location.reducers'
import weatherReducer from './weather.reducers'

const rootReducer = combineReducers({
  location: locationReducer,
  weather: weatherReducer
})

export default rootReducer
