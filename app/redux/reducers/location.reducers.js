import {
  FETCH_LOCATION_ID_BEGIN,
  FETCH_LOCATION_ID_SUCCESS,
  FETCH_LOCATION_ID_FAILURE
} from '../actions/location.actions'

const initialState = {
  data: [],
  loading: false,
  error: null
}
// return new state
const locationReducer = (state = initialState, action) => {
  // console.log('WEATHER REDUCER:', state, action)
  switch (action.type) {
    case FETCH_LOCATION_ID_BEGIN:
      console.log('fetch begin')
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_LOCATION_ID_SUCCESS:
      console.log('fetch success')
      return {
        loading: false,
        data: action.payload
      }
    case FETCH_LOCATION_ID_FAILURE:
      console.log('fetch error')
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    default:
      return state
  }
}

export default locationReducer
